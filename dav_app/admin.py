# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Students,Grade,Subject,Section
admin.site.register(Students)
admin.site.register(Grade)
admin.site.register(Section)
admin.site.register(Subject)
