# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from django.db import models

# Create your models here.
class Students(models.Model):
    firstname= models.CharField(max_length=100)
    lastname= models.CharField(max_length=100)
    email= models.CharField(max_length=200)

    def __str__(self):
        return self.firstname

class Grade(models.Model):
    grade = models.CharField(max_length=50)

    def __str__(self):
        return self.grade

class Section(models.Model):
    grade = models.ForeignKey('Grade',on_delete=models.CASCADE)
    section = models.CharField(max_length=50)

    def __str__(self):
        return self.section 

class Subject(models.Model):
    grade = models.ForeignKey('Grade',on_delete=models.CASCADE)
    section = models.ForeignKey('Section',on_delete=models.CASCADE)
    subject = models.CharField(max_length=50)

    def __str__(self):
        return self.subject       


